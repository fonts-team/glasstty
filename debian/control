Source: glasstty
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Adam Borowski <kilobyte@angband.pl>
Build-Depends: debhelper (>= 11~)
Standards-Version: 4.2.1
Rules-Requires-Root: no
Homepage: http://sensi.org/~svo/glasstty/
Vcs-Git: https://salsa.debian.org/fonts-team/glasstty
Vcs-Browser: https://salsa.debian.org/fonts-team/glasstty

Package: fonts-glasstty
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: VT220 terminal font
 This font faithfully reproduces the appearance of text on a DEC VT220
 terminal, up to scanline gaps.
 .
 As a pixel font, it looks good only at sizes that are 20 pixel tall (or
 1.5×, 2×, … that) -- on most setups this corresponds to "point" size
 of 15 (22.5, 30, …).  You might get reasonable results for other sizes
 only with VRGB/VBGR antialiasing (usu. portrait orientation screens),
 HiDPI, or bad eyes.
 .
 Supported characters include DEC Western, vt100 graphics, and Cyrillic.
